package com.desafio.indra.controller;

import com.desafio.indra.model.Combustivel;
import com.desafio.indra.response.DataColetaResponse;
import com.desafio.indra.response.RevendaInstalacaoResponse;
import com.desafio.indra.service.CombustivelService;
import com.desafio.indra.data.ImportacaoDados;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.desafio.indra.response.ValorMedioCompraVenda;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("combustivel")
@Api(value = "Combustivel")
public class CombustivelController {

    @Autowired
    private CombustivelService combustivelService;

    @Autowired
    private ImportacaoDados importacaoDados;

    @ApiOperation(value = "Retorna o Combustivel atravez do ID")
    @GetMapping(value = "/{idCombustivel}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Combustivel buscaCombustivel (@PathVariable Long idCombustivel) throws NotFoundException {

        return combustivelService.findById(idCombustivel);
    }

    @ApiOperation(value = "Busca todos os municípios existentes na tabela")
    @GetMapping(value = "/busca/municipio", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Combustivel> findAll () throws NotFoundException, IOException {

        return combustivelService.findAll();
    }

    @ApiOperation(value = "Retorna a média de preço referente a valores de compra por município")
    @GetMapping(value = "/buscar/valorCompra/{municipio}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Double getMediaValorCompraByMunicipio (@PathVariable String municipio) throws NotFoundException, IOException {

        return combustivelService.getMediaValorCompraByMunicipio(municipio);
    }

    @ApiOperation(value = "Retorna a média de preço referente a valores de venda por município")
    @GetMapping(value = "/buscar/valorVenda/{municipio}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Double getMediaValorVendaByMunicipio (@PathVariable String municipio) throws NotFoundException, IOException {

        return combustivelService.getMediaValorVendaByMunicipio(municipio);
    }

    @ApiOperation(value = "Retorna todos os registros cadastrados por sigla de região")
    @GetMapping(value = "/buscar/{regiao}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Combustivel> findAllByRegiao (@PathVariable String regiao) throws NotFoundException, IOException {

        return combustivelService.findAllByRegiao(regiao);
    }

    @ApiOperation(value = "Retorna todos os combustíveis cadastrados por distribuidora")
    @GetMapping(value = "/buscar/distribuidora/{distribuidora}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Combustivel> findAllByRevendaInstalacao (@PathVariable String distribuidora) throws NotFoundException, IOException {

        return combustivelService.findAllByRevendaInstalacao(distribuidora);
    }

    @ApiOperation(value = "Retorna todos os combustíveis agrupados por data de coleta")
    @GetMapping(value = "/buscar/dataColeta/{dataColeta}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Combustivel> findAllOrderByDataColeta (@PathVariable(value="dataColeta")
                                                      @DateTimeFormat(pattern="dd-MM-yyyy") String dataColeta) throws NotFoundException, IOException {

        return combustivelService.findAllByDataColeta(dataColeta);
    }

   @ApiOperation(value = "Retorna todos os combustíveis agrupados por data de coleta")
    @GetMapping(value = "/buscar/dataColeta", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<DataColetaResponse> findAllGroupByDataColeta () {
        return combustivelService.findAllGroupByDataColeta();
    }

    @ApiOperation(value = "Retorna todos os registros agrupados por distribuidora")
    @GetMapping(value = "/buscar/distribuidora", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<RevendaInstalacaoResponse> findAllGroupByDistribuidora () {
        return combustivelService.findAllGroupByRevendaInstalacao();
    }

    @ApiOperation(value = "Retorna todos o valor medio de compra e venda por Municipio")
    @GetMapping(value = "/valorMedio/compraVenda/municipio/{municipio}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ValorMedioCompraVenda valorMedioCompraVendaPorMunicipio (@PathVariable String municipio) {
        return combustivelService.findMediaValoresByMunicipio(municipio);
    }

    @ApiOperation(value = "Retorna todos o valor medio de compra e venda por Bandeira")
    @GetMapping(value = "/valorMedio/compraVenda/bandeira/{bandeira}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ValorMedioCompraVenda valorMedioCompraVendaPorBandeira (@PathVariable String bandeira) {
        return combustivelService.findMediaValoresByBandeira(bandeira);
    }

}
