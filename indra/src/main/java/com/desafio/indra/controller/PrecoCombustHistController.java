package com.desafio.indra.controller;

import com.desafio.indra.model.PrecoCombustivelHist;
import com.desafio.indra.request.PrecoRequest;
import com.desafio.indra.service.PrecoCombustivelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("precoCombustHist")
@Api(value = "Preco Combustivel Hist")
public class PrecoCombustHistController {

    @Autowired
    private PrecoCombustivelService precoCombustivelService;


    @ApiOperation(value = "Retorna uma lista de precos")
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<PrecoCombustivelHist> listaPreco (){
        return precoCombustivelService.findAll();
    }

    @ApiOperation(value = "Retorna o Preco atravez do ID")
    @GetMapping(value = "/{idPreco}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public PrecoCombustivelHist findPrecoById (@PathVariable Long idPreco) throws NotFoundException {
        return precoCombustivelService.findById(idPreco);
    }

    @ApiOperation(value = "Atualiza o Preco")
    @PutMapping(value = "/atualizar/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void updatePreco (@RequestBody PrecoRequest precoCombustivelHist, @PathVariable Long id) throws NotFoundException {
        precoCombustivelService.atualizaPrecoCombustivel(precoCombustivelHist,id);
    }

    @ApiOperation(value = "Salva o Preco")
    @PostMapping(value = "/salvar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void savePreco (@RequestBody PrecoRequest precoCombustivelHist) {
        precoCombustivelService.savePrecoCombustivel(precoCombustivelHist);
    }

    @ApiOperation(value = "Deleta o preco informado")
    @DeleteMapping(value = "/deletar/{idPreco}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void deletePreco (@PathVariable Long idPreco) throws NotFoundException {
        precoCombustivelService.deletaPrecoCombustivel(idPreco);
    }
}
