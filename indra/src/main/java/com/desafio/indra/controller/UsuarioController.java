package com.desafio.indra.controller;

import com.desafio.indra.model.Combustivel;
import com.desafio.indra.model.Usuario;
import com.desafio.indra.service.UsuarioService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("usuario")
@Api(value = "Usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @ApiOperation(value = "Retorna uma lista de usuários")
    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Usuario> listaUsuario (){
        return usuarioService.listaUsuarios();
    }

    @ApiOperation(value = "Retorna o usuário atravez do ID")
    @GetMapping(value = "/{idUsuario}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Usuario buscaUsuario (@PathVariable Long idUsuario) throws NotFoundException {
        return usuarioService.buscaUsuario(idUsuario);
    }

    @ApiOperation(value = "Atualiza o usuário")
    @PutMapping(value = "/atualizar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void atualizaUsuario (@RequestBody Usuario usuario) throws NotFoundException {
        usuarioService.atualizaUsuario(usuario);
    }

    @ApiOperation(value = "Salva o usuário")
    @PostMapping(value = "/salvar", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void salvaUsuario (@RequestBody Usuario usuario) {
        usuarioService.salvarUsuario(usuario);
    }

    @ApiOperation(value = "Deleta o usuário informado")
    @DeleteMapping(value = "/deletar/{idUsuario}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void deletaUsuario (@PathVariable Long idUsuario) throws NotFoundException {
        usuarioService.deletaUsuario(idUsuario);
    }
}
