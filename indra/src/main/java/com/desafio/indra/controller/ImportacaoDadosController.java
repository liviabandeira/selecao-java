package com.desafio.indra.controller;

import com.desafio.indra.data.ImportacaoDados;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("dados")
@Api(value = "Dados")
public class ImportacaoDadosController {

    @Autowired
    private ImportacaoDados importacaoDados;

    @ApiOperation(value = "Ler e preenche os dados na tabela Combustivel")
    @GetMapping(value = "/lerArquivo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void lerArquivo () throws NotFoundException, IOException {
        importacaoDados.importacaoArquivo();
    }
}
