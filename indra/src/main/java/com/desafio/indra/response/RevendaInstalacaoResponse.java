package com.desafio.indra.response;

import com.desafio.indra.model.Combustivel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RevendaInstalacaoResponse {

    private String revendaInstalacao;

    private List<Combustivel> combustivelList;
}
