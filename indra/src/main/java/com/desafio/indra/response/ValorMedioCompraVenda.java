package com.desafio.indra.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ValorMedioCompraVenda {

    private Double valorMedioCompra;
    private Double valorMedioVenda;
}
