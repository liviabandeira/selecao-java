package com.desafio.indra.service;

import com.desafio.indra.model.Combustivel;
import com.desafio.indra.repository.CombustivelRepository;
import com.desafio.indra.response.DataColetaResponse;
import com.desafio.indra.response.RevendaInstalacaoResponse;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.desafio.indra.response.ValorMedioCompraVenda;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class CombustivelService {

    private static final Double ZERO = 0.0;

    @Autowired
    private CombustivelRepository combustivelRepository;

    public Combustivel findById (Long id) throws NotFoundException {
        return combustivelRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Combustivel de ID " + id + "não foi encontrado"));
    }

    public Double getMediaValorCompraByMunicipio (String municipio) {
        return combustivelRepository.getMediaValorCompraByMunicipio(municipio);
    }

    public Double getMediaValorVendaByMunicipio (String municipio) {
        return combustivelRepository.getMediaValorVendaByMunicipio(municipio);
    }

    public List<Combustivel> findAllByRegiao (String regiao) {
        return combustivelRepository.findAllByRegiao(regiao);
    }

    public List<Combustivel> findAll () {
        return combustivelRepository.findAll();
    }

    public List<Combustivel> findAllByRevendaInstalacao (String revendaInstalacao) {
        return combustivelRepository.findAllByRevendaInstalacao(revendaInstalacao);
    }

    public List<Combustivel> findAllByDataColeta (String dataColeta) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate data = LocalDate.parse(dataColeta, formatter);

        return combustivelRepository.findAllByDataColetaOrderByDataColetaDesc(data);
    }

    public ValorMedioCompraVenda findMediaValoresByMunicipio (String municipio) {

        return combustivelRepository.findAvgValorMunicipio(municipio);
    }

    public ValorMedioCompraVenda findMediaValoresByBandeira (String bandeira) {

        return combustivelRepository.findAvgValorBandeira(bandeira);
    }

    public List<RevendaInstalacaoResponse> findAllGroupByRevendaInstalacao () {

        Boolean aux = false;
        List<Combustivel> all = combustivelRepository.findAll();
        List<RevendaInstalacaoResponse> revendaInstalacaoResponses = new ArrayList<>();

        for(Combustivel combustivel : all) {

            for (RevendaInstalacaoResponse revendaInstalacaoResponse : revendaInstalacaoResponses){
                aux = false;
                if (combustivel.getRevendaInstalacao().equals(revendaInstalacaoResponse.getRevendaInstalacao())){
                    revendaInstalacaoResponse.getCombustivelList().add(combustivel);
                    aux = true;
                    break;
                }
            }

            if (!aux) {
                RevendaInstalacaoResponse revendaInstalacaoResponseTemp = new RevendaInstalacaoResponse(combustivel.getRevendaInstalacao(), new ArrayList<>());
                revendaInstalacaoResponseTemp.getCombustivelList().add(combustivel);
                revendaInstalacaoResponses.add(revendaInstalacaoResponseTemp);

            }
        }

        return revendaInstalacaoResponses;

    }

    public List<DataColetaResponse> findAllGroupByDataColeta () {
        Boolean aux = false;
        List<Combustivel> all = combustivelRepository.findAll();
        List<DataColetaResponse> dataColetaResponses = new ArrayList<>();

        for(Combustivel combustivel : all) {

            for (DataColetaResponse dataColetaResponse : dataColetaResponses){
                aux = false;
                if (combustivel.getDataColeta().isEqual(dataColetaResponse.getDataColeta())){
                    dataColetaResponse.getCombustivelList().add(combustivel);
                    aux = true;
                    break;
                }
            }

            if (!aux) {
                DataColetaResponse dataColetaResponseTemp = new DataColetaResponse(combustivel.getDataColeta(), new ArrayList<>());
                dataColetaResponseTemp.getCombustivelList().add(combustivel);
                dataColetaResponses.add(dataColetaResponseTemp);

            }
        }

        return dataColetaResponses;

    }

}
