package com.desafio.indra.service;

import com.desafio.indra.model.Usuario;
import com.desafio.indra.repository.UsuarioRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<Usuario> listaUsuarios() {
        return usuarioRepository.findAll();
    }

    public void salvarUsuario (Usuario usuario) {
        usuarioRepository.save(usuario);
    }

    public void atualizaUsuario (Usuario usuario) throws NotFoundException {
        Usuario oldUsuario = usuarioRepository.findById(usuario.getId()).orElseThrow(() -> new NotFoundException("Usuario não encontrado"));
        Usuario usuarioAtualizado = oldUsuario.builder().email(usuario.getEmail())
                .idade(usuario.getIdade())
                .nome(usuario.getNome())
                .telefone(usuario.getTelefone())
                .id(usuario.getId()).build();

        usuarioRepository.save(usuarioAtualizado);
    }

    public void deletaUsuario (Long idUsuario) throws NotFoundException {
        Usuario usuarioDeletar = usuarioRepository.findById(idUsuario).orElseThrow(() -> new NotFoundException("Usuario não encontrado"));
        usuarioRepository.delete(usuarioDeletar);
    }

    public Usuario buscaUsuario (Long idUsuario) throws NotFoundException {
        return usuarioRepository.findById(idUsuario).orElseThrow(() -> new NotFoundException("Usuario não encontrado"));
    }
}