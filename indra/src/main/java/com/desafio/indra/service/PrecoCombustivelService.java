package com.desafio.indra.service;

import com.desafio.indra.model.PrecoCombustivelHist;
import com.desafio.indra.repository.PrecoCombustivelHistRepository;
import com.desafio.indra.request.PrecoRequest;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PrecoCombustivelService {

    @Autowired
    private PrecoCombustivelHistRepository precoCombustivelHistRepository;

    public List<PrecoCombustivelHist> findAll () {
        return precoCombustivelHistRepository.findAll();
    }

    public void savePrecoCombustivel (PrecoRequest precoCombustivelHist) {
        Double preco = Double.parseDouble(precoCombustivelHist.getPreco());
        PrecoCombustivelHist precoCombustivel = new PrecoCombustivelHist(null, precoCombustivelHist.getDescricao(),
                preco, LocalDate.now(), null,null,precoCombustivelHist.getUsuario());
        precoCombustivelHistRepository.save(precoCombustivel);
    }

    public void atualizaPrecoCombustivel (PrecoRequest precoCombustivelHist, Long id) throws NotFoundException{
        PrecoCombustivelHist oldPreco = precoCombustivelHistRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Entidade não encontrada"));

        Double preco = Double.parseDouble(precoCombustivelHist.getPreco());
        PrecoCombustivelHist precoAtualizado = oldPreco.builder().preco(preco)
                .dataAlteracao(LocalDate.now())
                .descricao(precoCombustivelHist.getDescricao())
                .usuarioAlteracao(precoCombustivelHist.getUsuario())
                .id(id).build();

        precoCombustivelHistRepository.save(precoAtualizado);

    }

    public void deletaPrecoCombustivel (Long id) {
        precoCombustivelHistRepository.deleteById(id);
    }

    public PrecoCombustivelHist findById (Long id) throws NotFoundException {
        return precoCombustivelHistRepository.findById(id).orElseThrow(() -> new NotFoundException("Entidade não encontrada"));
    }
}
