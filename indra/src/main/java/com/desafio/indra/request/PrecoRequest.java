package com.desafio.indra.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PrecoRequest {

    private String preco;
    private String descricao;
    private String usuario;

}
