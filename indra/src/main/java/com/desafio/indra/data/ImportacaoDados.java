package com.desafio.indra.data;

import com.desafio.indra.model.Combustivel;
import com.desafio.indra.repository.CombustivelRepository;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class ImportacaoDados {

    private static final Double ZERO = 0.0;
    @Autowired
    private CombustivelRepository combustivelRepository;

    public void importacaoArquivo () throws IOException {

        Reader reader = Files.newBufferedReader(Paths.get("combustiveis.csv"));
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

        List<String[]> combustiveis = csvReader.readAll();

        for (String[] combustivel : combustiveis) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate data = LocalDate.parse(combustivel[6], formatter);
            System.out.println(data);

            double valorVenda = ZERO;
            double valorCompra = ZERO;

            if(!combustivel[8].isEmpty()) {
                 valorVenda = Double.parseDouble(combustivel[8]);
            }
            if(!combustivel[7].isEmpty()) {
                valorCompra = Double.parseDouble(combustivel[7]);
            }

            Combustivel novoCombustivel = new Combustivel(null, combustivel[0], combustivel[1], combustivel[2],
                    combustivel[3], combustivel[4], combustivel[5], data, valorCompra,
                    valorVenda, combustivel[9],combustivel[10]);

            combustivelRepository.save(novoCombustivel);
        }
}
}
