package com.desafio.indra.repository;

import com.desafio.indra.model.Combustivel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.desafio.indra.response.ValorMedioCompraVenda;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

@Repository
public interface CombustivelRepository extends JpaRepository <Combustivel, Long> {

    @Query(value = "SELECT avg(valorCompra) FROM Combustivel WHERE municipio = ?1")
    Double getMediaValorCompraByMunicipio (String municipio);

    @Query(value = "SELECT avg(valorVenda) FROM Combustivel WHERE municipio = ?1")
    Double getMediaValorVendaByMunicipio (String municipio);

    List<Combustivel> findAllByRegiao (String regiao);

    List<Combustivel> findAllByRevendaInstalacao (String revendaInstalacao);

    List<Combustivel> findAllByDataColetaOrderByDataColetaDesc (LocalDate dataColeta);

    @Query(value = "SELECT c FROM Combustivel c GROUP BY revendaInstalacao")
    List<Combustivel> findAllByRevendaInstalacao();

    @Query(value = "SELECT c FROM Combustivel c GROUP BY dataColeta")
    List<Combustivel> findAllGroupByDataColeta();

    @Query(value = "SELECT new com.desafio.indra.response.ValorMedioCompraVenda(avg(valorCompra) as valorMedioCompra, avg(valorVenda) as valorMedioVenda) FROM Combustivel WHERE municipio = ?1")
    ValorMedioCompraVenda findAvgValorMunicipio(String municipio);

    @Query(value = "SELECT new com.desafio.indra.response.ValorMedioCompraVenda(avg(valorCompra) as valorMedioCompra, avg(valorVenda) as valorMedioVenda) FROM Combustivel WHERE bandeira = ?1")
    ValorMedioCompraVenda findAvgValorBandeira(String bandeira);

}
