package com.desafio.indra.repository;

import com.desafio.indra.model.PrecoCombustivelHist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrecoCombustivelHistRepository extends JpaRepository<PrecoCombustivelHist, Long> {
}
