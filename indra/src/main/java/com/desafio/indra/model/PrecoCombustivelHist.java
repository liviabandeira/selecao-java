package com.desafio.indra.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrecoCombustivelHist {

    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "Descricao")
    private String descricao;

    @Column(name = "Preco")
    private Double preco;

    @Column(name = "DataInclusao")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dataInclusao;

    @Column(name = "DataAlteracao")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dataAlteracao;

    @Column(name = "UsuarioAlteracao")
    private String usuarioAlteracao;

    @Column(name = "UsuarioInclusao")
    private String usuarioInclusao;

}
