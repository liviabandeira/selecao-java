package com.desafio.indra.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Combustivel {

    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "Regiao")
    private String regiao;

    @Column(name = "SiglaEstado ")
    private String siglaEstado;

    @Column(name = "SiglaMunicipio")
    private String municipio;

    @Column(name = "RevendaInstalacao")
    private String revendaInstalacao;

    @Column(name = "Codigo")
    private String codigo;

    @Column(name = "Produto")
    private String produto;

    @Column(name = "DataColeta")
    @JsonFormat(pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dataColeta;

    @Column(name = "VALORCOMPRA")
    private Double valorCompra;

    @Column(name = "VALORVENDA")
    private Double valorVenda;

    @Column(name = "UnidadeMedida")
    private String unidadeMedida;

    @Column(name = "Bandeira")
    private String bandeira;

}
